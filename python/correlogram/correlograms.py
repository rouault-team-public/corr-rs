"""Define correlogram functions."""

from __future__ import annotations

import numpy as np
from numpy import typing as npt
from scipy import stats

Afloat = npt.NDArray[np.float64]


def corr_shuffled(
    sp_times1: Afloat, sp_times2: Afloat, dt: float
) -> tuple[float, float, float]:
    """Compute the distribution parameters for the shuffled trains.

    Args:
        sp_times1 (Afloat): train 1
        sp_times2 (Afloat): train 2
        dt (float): bin width

    Returns:
        float: mean
        float: lower confidence interval
        float: higher confidence interval

    """
    mu = sp_times1.size * sp_times2.size * dt / sp_times2[-1]
    poisson_dist = stats.poisson(mu)

    low_ppf = poisson_dist.ppf(0.01)
    high_ppf = poisson_dist.ppf(0.99)

    return mu, low_ppf, high_ppf
