"""Define the module."""

from __future__ import annotations

from pathlib import Path
from typing import TYPE_CHECKING

from correlogram.utils import parse_into_expr

if TYPE_CHECKING:
    import polars as pl
    from polars.type_aliases import IntoExpr


lib = Path(__file__).parent


def autocorr(
    expr: IntoExpr, other: IntoExpr, width: float, step: float, corr_type: str
) -> pl.Expr:
    """Expression plugin for autocorrelogram.

    Args:
        expr: the cluster indices (the autocorrelogram is not computed for null values)
        other: the cluster spike times
        width: histogram half width (ms)
        step: bin size (ms)
        corr_type: correlation type "Histo" or "Raster"

    Returns:
        the autocorrelogram column
    """
    expr = parse_into_expr(expr)
    return expr.register_plugin(
        lib=lib,
        symbol="autocorr",
        is_elementwise=True,
        args=[other],
        kwargs={"width": width, "step": step, "corr_type": corr_type},
    )


def pair_corr(
    expr: IntoExpr, other: IntoExpr, width: float, step: float, corr_type: str
) -> pl.Expr:
    """Expression plugin for pair correlogram.

    Args:
        expr: the cluster indices (the crosscorrelogram is not computed against
              the null values)
        other: the cluster spike times
        width: histogram half width (ms)
        step: bin size (ms)
        corr_type: correlation type "Histo" or "Raster"

    Returns:
        the pair correlogram column
    """
    expr = parse_into_expr(expr)
    return expr.register_plugin(
        lib=lib,
        symbol="pair_corr",
        is_elementwise=False,
        args=[other],
        kwargs={"width": width, "step": step, "corr_type": corr_type},
    )


def interaction_type(
    expr: IntoExpr,
    n_prod: IntoExpr,
    duration: IntoExpr,
    width: float,
    step: float,
) -> pl.Expr:
    """Expression plugin for annotating interactions.

    Args:
        expr: the spike times
        n_prod: the product of the number of spikes of the two clusters
        duration: recording time length
        width: histogram half width (ms)
        step: bin size (ms)

    Returns:
        the annotation column in JSON format
    """
    expr = parse_into_expr(expr)
    return expr.register_plugin(
        lib=lib,
        symbol="interaction_type",
        is_elementwise=True,
        args=[n_prod, duration],
        kwargs={"width": width, "step": step},
    )
