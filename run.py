"""Test the minimal plugin."""
import logging

import correlogram as correl
import polars as pl

logging.basicConfig(level=logging.INFO)

inds = pl.Series("inds", [3, 6], dtype=pl.UInt32)
spikes1 = pl.Series("times", [[1.2, 1.3], [5.1, None]])

df_spk = pl.DataFrame([inds, spikes1])

histo = df_spk.with_columns(
    autocorr=correl.autocorr("times", width=50.0, step=1.0, corr_type="Histo")
)
logging.info(histo)

histo_pair = df_spk.with_columns(
    corr_pair=correl.pair_corr(
        "inds", "times", width=50.0, step=1.0, corr_type="Raster"
    )
)
logging.info(histo_pair)
