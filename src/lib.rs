#![allow(clippy::style)]
#![allow(clippy::pedantic)]

use pyo3::types::PyModule;
use pyo3::{pymodule, PyResult, Python};
use pyo3_polars::derive::polars_expr;
use rayon::prelude::*;
use serde::{Deserialize, Serialize};

use statrs::distribution::{DiscreteCDF, Poisson};

use polars::prelude::*;

fn build_bins(width: f64, step: f64) -> Vec<f32> {
    let n_side = (width / step) as i32;
    let n_bin = 2 * n_side + 1;
    (0..n_bin)
        .map(|x| ((x - n_side) as f64 * step) as f32)
        .collect()
}

fn counts_update(counts: &mut [u32], bins: &[f32], times: &[f32]) {
    let mut bin_iter = bins.iter();
    let mut bin_lim = bin_iter.next().unwrap();
    let mut offset = 0usize;

    for ti in times.iter() {
        loop {
            if ti < bin_lim {
                counts[offset] += 1;
                break;
            }
            match bin_iter.next() {
                Some(val) => bin_lim = val,
                None => break,
            }
            offset += 1;
        }
    }
}

fn loop_limit<'a, I>(val: &mut f64, val_comp: f64, offset: &mut usize, iter: &mut I)
where
    I: Iterator<Item = &'a f64>,
{
    loop {
        if *val < val_comp {
            *offset += 1;
            match iter.next() {
                Some(v) => *val = *v,
                None => {
                    *val = f64::INFINITY;
                    break;
                }
            }
        } else {
            break;
        }
    }
}

#[derive(Clone, Copy, Debug, Deserialize)]
enum CorrType {
    Raster,
    Histo,
}

#[derive(Deserialize)]
struct CorrProp {
    width: f64,
    step: f64,
    corr_type: CorrType,
}

fn list_corr_dtype(input_fields: &[Field]) -> PolarsResult<Field> {
    let field = Field::new(
        input_fields[0].name(),
        DataType::List(Box::new(DataType::Float32)),
    );
    Ok(field.clone())
}

fn pair_corr_vecs(
    spikes1: &Vec<f64>,
    spikes2: Option<&Vec<f64>>,
    bins: &Vec<f32>,
    corr_type: CorrType,
) -> PolarsResult<Series> {
    let mut offset_left = 0usize;
    let mut offset_right = 0usize;

    let mut auto_corr = false;
    let spikes2 = {
        if let Some(spikes) = spikes2 {
            spikes
        } else {
            auto_corr = true;
            spikes1
        }
    };

    let mut iter_l = spikes2.iter();
    let vall = iter_l.next();
    let mut vall = match vall {
        Some(i) => *i,
        None => match corr_type {
            CorrType::Raster => {
                return Ok(Series::new_empty(
                    "",
                    &DataType::List(Box::new(DataType::Float32)),
                ));
            }
            CorrType::Histo => {
                return Ok(Series::new_empty("", &DataType::UInt32));
            }
        },
    };
    let mut iter_r = spikes2.iter();
    let valr = iter_r.next();
    let mut valr = *valr.unwrap();

    let bin0 = bins[0] as f64;
    let binlast = bins[bins.len() - 1] as f64;

    let mut bin_counts = vec![0; bins.len()];
    let mut raster: Vec<Option<Series>> = Vec::new();

    for (i_val, val) in spikes1.into_iter().enumerate() {
        let val0 = bin0 + val;
        let vallast = binlast + val;

        loop_limit(&mut vall, val0, &mut offset_left, &mut iter_l);
        loop_limit(&mut valr, vallast, &mut offset_right, &mut iter_r);

        if offset_right > offset_left {
            let slice = &spikes2[offset_left..offset_right];
            let mut slice_vec: Vec<f32> = slice.iter().map(|b| (b - val) as f32).collect();

            if auto_corr {
                slice_vec.remove(i_val - offset_left);
            }
            match corr_type {
                CorrType::Raster => {
                    raster.push(Some(slice_vec.into_iter().collect()));
                }
                CorrType::Histo => {
                    counts_update(&mut bin_counts, &bins, &slice_vec);
                }
            }
        }
    }
    match corr_type {
        CorrType::Raster => {
            if raster.len() == 0 {
                raster.push(Some(Series::new_empty("", &DataType::Float32)));
            }
            let chunk_out: ListChunked = raster.into_iter().collect();
            Ok(chunk_out.into_series())
        }
        CorrType::Histo => {
            let counts_out: Vec<u32> = bin_counts[1..bin_counts.len()].to_vec();
            Ok(Series::new("counts", counts_out))
        }
    }
}

fn pair_corrs(
    spikes: &Float64Chunked,
    ind: Option<u32>,
    spikes_others: &Vec<Option<Series>>,
    clu_inds: &Vec<Option<u32>>,
    bins: &Vec<f32>,
    corr_type: CorrType,
) -> PolarsResult<ListChunked> {
    let spikes_vec: Vec<f64> = spikes.iter().flatten().collect();

    spikes_others
        .iter()
        .zip(clu_inds)
        .map(|(opt_spks_oth, ind_oth)| {
            if ind_oth == &ind {
                return Ok(None);
            }
            let spks_oth: Vec<f64> = {
                if let Some(spks_oth) = opt_spks_oth {
                    spks_oth.f64()?.iter().flatten().collect()
                } else {
                    return Ok(None);
                }
            };
            Ok(Some(pair_corr_vecs(
                &spikes_vec,
                Some(&spks_oth),
                &bins,
                corr_type,
            )?))
        })
        .collect()
}

#[derive(Clone, Copy, Debug, Deserialize)]
enum InteractionType {
    Raster,
    Histo,
}

#[derive(Deserialize)]
struct InterProp {
    step: f64,
}

#[derive(Debug, Serialize)]
enum InterType {
    Excitation {
        ampl: f64,
        time: f64,
    }, // amplitude and time
    CoFiring,
    Inhibition {
        trough_time: f64,
        trough_area: f64,
        trough_dur: f64,
    },
    GlobalInhib {
        trough_time: f64,
    },
    NotEnoughSpk,
    NoEffect,
}

#[derive(Debug, Serialize)]
struct ConfInterval {
    inf: u64,
    mean: f64,
    sup: u64,
}

#[derive(Debug, Serialize)]
struct PairAnnotation {
    conf_int: ConfInterval,
    inter_type: InterType,
}

fn conf_interval(mu: f64) -> ConfInterval {
    let poiss = Poisson::new(mu).unwrap();

    // very unefficient solution but the implementation in statrs is broken...
    let max_out = 100000;

    let thr_inf = 0.01;
    let thr_sup = 0.99;
    let max_inf = mu.floor() as u64;
    let mut i_inf = max_inf;
    let mut i_sup = max_out;
    for i in 0..max_inf {
        if poiss.cdf(i) > thr_inf {
            i_inf = i;
            break;
        }
    }
    for i in max_inf..max_out {
        if poiss.cdf(i) > thr_sup {
            i_sup = i;
            break;
        }
    }
    i_sup -= 1;

    ConfInterval {
        inf: i_inf,
        mean: mu,
        sup: i_sup,
    }
}

fn get_max(nets: &[u32]) -> Option<(usize, u32)> {
    let retmax = nets.iter().enumerate().max_by(|(_, a), (_, b)| a.cmp(b));
    if let Some((ind, val)) = retmax {
        Some((ind, *val))
    } else {
        None
    }
}

fn get_min(nets: &[u32]) -> Option<(usize, u32)> {
    let retmin = nets.iter().enumerate().min_by(|(_, a), (_, b)| a.cmp(b));
    if let Some((ind, val)) = retmin {
        Some((ind, *val))
    } else {
        None
    }
}

fn pair_confidence_inter(histo: &[u32], n_prod: f32, dur: f64, props: &InterProp) -> ConfInterval {
    let tot_spike: u64 = histo.iter().map(|x| *x as u64).sum();
    let mu = if tot_spike > 100 {
        tot_spike as f64 / histo.len() as f64
    } else {
        n_prod as f64 * props.step / dur
    };
    conf_interval(mu)
}

fn interaction_type_series(
    histo: &[u32],
    conf_inter: &ConfInterval,
    props: &InterProp,
) -> InterType {
    // We assume that the histogram is centered
    let mid_zone = histo.len() / 2;
    let histo_pos = &histo[mid_zone..];

    let exc_zone_l = 3usize;
    let exc_len_thr = 3usize;

    let inh_zone_l = 4usize;
    let inh_len_thr = 3u32;

    let glob_inh_time_thr = 2u32;

    // find the excitatory peak
    let exc_zone = &histo_pos[..exc_zone_l];
    let (ind_peak, ampl_peak) = get_max(exc_zone).unwrap();
    let mut peak_type = None;
    if ampl_peak > conf_inter.sup as u32 {
        let zone_after = &histo_pos[ind_peak..ind_peak + exc_len_thr];
        if zone_after
            .iter()
            .map(|x| (*x <= conf_inter.sup as u32) as u32)
            .sum::<u32>()
            > 0
        {
            peak_type = Some((ampl_peak, ind_peak));
        }
    };

    if let Some(a_p) = peak_type {
        return InterType::Excitation {
            ampl: a_p.0 as f64,
            time: a_p.1 as f64 * props.step,
        };
    }
    let cofiring_zone = &histo[mid_zone - 1..mid_zone + 1];
    if cofiring_zone
        .iter()
        .map(|x| (*x <= conf_inter.sup as u32) as u32)
        .sum::<u32>()
        == 0
    {
        return InterType::CoFiring;
    }

    // find the inhibitory peak
    let inh_zone = &histo_pos[..inh_zone_l];
    let (ind_trough, _ampl_trough) = get_min(inh_zone).unwrap();

    let n_signif_inh = inh_zone
        .iter()
        .map(|x| (*x < conf_inter.inf as u32) as u32)
        .sum::<u32>();
    if n_signif_inh >= inh_len_thr {
        let start_inh = histo_pos
            .iter()
            .position(|x| *x < conf_inter.inf as u32)
            .unwrap();
        let inh_dur = &histo_pos[start_inh..]
            .iter()
            .position(|x| *x >= conf_inter.inf as u32);
        let inh_dur = inh_dur.unwrap_or(histo_pos.len() - start_inh);
        let trough_area = &histo_pos[start_inh..start_inh + inh_dur]
            .iter()
            .sum::<u32>()
            - conf_inter.inf as u32 * inh_dur as u32;

        return InterType::Inhibition {
            trough_time: ind_trough as f64 * props.step,
            trough_area: trough_area as f64 * props.step,
            trough_dur: inh_dur as f64 * props.step,
        };
    }

    // global inhibition
    let coarse_fact: u32 = 5;
    let mu_coarse = coarse_fact as f64 * conf_inter.mean;
    let conf_coarse = conf_interval(mu_coarse);
    if inh_zone
        .iter()
        .map(|x| (*x <= conf_inter.inf as u32) as u32)
        .sum::<u32>()
        >= inh_len_thr
    {
        // building the first bin of a coarse grained
        let bin_val: u32 = histo_pos[..coarse_fact as usize].iter().sum::<u32>();
        if (bin_val < conf_coarse.inf as u32) & (ind_trough as u32 <= glob_inh_time_thr) {
            return InterType::GlobalInhib {
                trough_time: ind_trough as f64 * props.step,
            };
        }
    }
    if conf_coarse.inf == 0 {
        return InterType::NotEnoughSpk;
    }

    InterType::NoEffect
}

#[polars_expr(output_type=String)]
fn interaction_type(inputs: &[Series], kwargs: InterProp) -> PolarsResult<Series> {
    let histo = inputs[0].list()?;
    let n_spk_prod = inputs[1].f32()?;
    let duration = inputs[2].f64()?;

    let struct_out: StringChunked = histo
        .into_no_null_iter()
        .zip(n_spk_prod.into_no_null_iter())
        .zip(duration.into_no_null_iter())
        .map(|((hi, n_prod), dur)| {
            let histo: Vec<u32> = hi.u32().unwrap().iter().flatten().collect();
            // TODO: do it cleaner!
            if histo.len() == 0 {
                return None;
            };
            let conf_inter = pair_confidence_inter(&histo, n_prod, dur, &kwargs);
            let inter_type = interaction_type_series(&histo, &conf_inter, &kwargs);
            let pair_annot = PairAnnotation {
                conf_int: conf_inter,
                inter_type,
            };
            let str_out = serde_json::to_string(&pair_annot).unwrap();
            Some(str_out)
        })
        .collect();

    Ok(struct_out.into_series())
}

#[polars_expr(output_type_func=list_corr_dtype)]
fn autocorr(inputs: &[Series], kwargs: CorrProp) -> PolarsResult<Series> {
    let bins = build_bins(kwargs.width, kwargs.step);

    let clu_inds = inputs[0].u32()?;
    let spikes = inputs[1].list()?;
    let spikes_vec: Vec<Option<Series>> = spikes.into_iter().collect();

    let clu_inds: Vec<Option<u32>> = clu_inds.iter().collect();
    let out: PolarsResult<ListChunked> = spikes_vec
        .par_iter()
        .zip(clu_inds.par_iter())
        .map(|(opt_s, opt_ind)| {
            if opt_ind.is_none() {
                return Ok(None);
            };
            let s: &Series = {
                if let Some(s) = opt_s {
                    s
                } else {
                    return Ok(None);
                }
            };
            let ca: &Float64Chunked = s.f64()?;
            let ca: Vec<f64> = ca.iter().flatten().collect();
            Ok(Some(
                pair_corr_vecs(&ca, None, &bins, kwargs.corr_type)?.into_series(),
            ))
        })
        .collect();

    Ok(out?.into_series())
}

#[polars_expr(output_type_func=list_corr_dtype)]
fn pair_corr(inputs: &[Series], kwargs: CorrProp) -> PolarsResult<Series> {
    let bins = build_bins(kwargs.width, kwargs.step);

    let clu_inds = inputs[0].u32()?;
    let spikes = inputs[1].list()?;

    let spikes_vec: Vec<Option<Series>> = spikes.into_iter().collect();

    let clu_inds: Vec<Option<u32>> = clu_inds.iter().collect();
    let out: PolarsResult<ListChunked> = spikes_vec
        .par_iter()
        .zip(clu_inds.par_iter())
        .map(|(opt_s, opt_ind)| {
            if opt_ind.is_none() {
                return Ok(None);
            };
            let s: &Series = {
                if let Some(s) = opt_s {
                    s
                } else {
                    return Ok(None);
                }
            };
            let ca: &Float64Chunked = s.f64()?;
            Ok(Some(
                pair_corrs(
                    ca,
                    *opt_ind,
                    &spikes_vec,
                    &clu_inds,
                    &bins,
                    kwargs.corr_type,
                )?
                .into_series(),
            ))
        })
        .collect();

    Ok(out?.into_series())
}

#[pymodule]
fn correlogram(_py: Python, m: &PyModule) -> PyResult<()> {
    // m.add_function(wrap_pyfunction!(autocorr, m)?)?;
    // m.add_function(wrap_pyfunction!(correl, m)?)?;
    m.add("__version__", env!("CARGO_PKG_VERSION"))?;
    Ok(())
}
